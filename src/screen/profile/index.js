/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    Button,
    Image
} from 'react-native';

import images from '../../../assets/images.js';

const instructions = Platform.select({
    ios: 'Press Cmd+R to reload,\n' +
    'Cmd+D or shake for dev menu',
    android: 'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

type Props = {};
export default class App extends Component<Props> {
    constructor(props){
        super(props)
        this.state ={
            no : this.props.navigation.state.params.no
        }

        if (this.props.navigation.state.params) {
            // alert(JSON.stringify(this.props.navigation.state.params.no))
            this.setState({no : this.props.navigation.state.params.no})
        }

    }

    onclick = () => {
        this.props.navigation.navigate('Home');
    }

    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.welcome}>
                   {"Profile "+this.state.no}
        </Text>

                <Button
                    onPress={this.onclick}
                    title="Learn More"
                    color="#841584"
                    accessibilityLabel="Learn more about this purple button" />
                    <Image source={images.albumArt1}/>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
});
